////  WebViewViewController.swift
//  Swift5WKWebView
//
//  Created on 13/10/2020.
//  
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    private let url: URL
    
    init(url: URL, title: String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let webView: WKWebView = {
        let preferences = WKWebpagePreferences()
        preferences.allowsContentJavaScript = true
        let configuration = WKWebViewConfiguration()
        configuration.defaultWebpagePreferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(webView)
        webView.frame = view.bounds
        
        webView.load(URLRequest(url: url))
        
        configureButtons()
    }

    private func configureButtons() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(didDoneButtonTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action:  #selector(didRefreshButtonTapped))
    }
    
    @objc private func didDoneButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func didRefreshButtonTapped(){
        webView.load(URLRequest(url: url))
    }
    
}
