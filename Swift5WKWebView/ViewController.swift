////  ViewController.swift
//  Swift5WKWebView
//
//  Created on 13/10/2020.
//  
//

import UIKit

class ViewController: UIViewController {

    private let button: UIButton = {
        let b = UIButton()
        b.backgroundColor = .systemGreen
        b.setTitle("Show webPage", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.layer.cornerRadius = 9
        b.layer.masksToBounds = true
        b.frame = CGRect(x: 0, y: 0, width: 220, height: 55)
        b.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.center = view.center
        view.addSubview(button)
    }

    @objc private func didTapButton() {
        guard let url = URL(string: "https://www.google.pl") else { return }
        
        let vc = WebViewViewController(url: url, title: "Google")
        let navVC = UINavigationController(rootViewController: vc)
        present(navVC, animated: true)
    }
    
}

